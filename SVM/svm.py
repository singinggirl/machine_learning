# -*- coding: utf-8 -*-
from libsvm.svm import *
from libsvm.svmutil import *
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
import seaborn as sns


#调整参数
def tuning_gauss(x: list, y: list, c_range: np.ndarray, g_range: np.ndarray):
    """
    SVM高斯核调参

    Args:
        x (list): 数据特征
        y (list): 数据标签
        c_range (np.ndarray): c参数所有取值
        g_range (np.ndarray): g参数所有取值

    Returns:
        best_result (dict): 调参的最优结果，包含精度和c、g取值
        result_frame (pd.DataFrame): 调参过程中所有c、g和对应精度
    """
    best_result = {"Accuracy": -1, "c": -1, "g": -1}
    result_file_name = "machine_learning/SVM/best_result.txt"
    result_array = []
    clear_file(result_file_name)
    for c in c_range:
        for g in g_range:
            param_str = '-q -t 2 -c ' + str(c) + ' -g ' + str(g)
            accuracy = leave_one_out(x, y, param_str)
            result_array.append([float(format(c, '.6f')), float(format(g, '.6f')), accuracy])
            if accuracy >= best_result["Accuracy"]:
                best_result["Accuracy"] = accuracy
                best_result["c"] = c
                best_result["g"] = g
                append_dict_to_file(result_file_name, best_result)
    result_frame = pd.DataFrame(result_array, columns=['c', 'g', 'Accuracy'])
    return best_result, result_frame
   
 
def clear_file(filename: str):
    """
    清空文件

    Args:
        filename (str): 文件名

    Returns:
        None
    """
    with open(filename, mode='r+', encoding='UTF-8') as file_object:
        file_object.truncate()   

def leave_one_out(x: list, y: list, param_str: str):
    """
    进行留一交叉验证

    Args:
        x (list): 数据特征
        y (list): 数据标签
        param_str (str): SVM参数指令

    Returns:
        留一交叉验证精度
    """
    param_str += " -v " + str(len(y))
    accuracy = svm_train(y, x, param_str)
    return accuracy


def append_dict_to_file(filename: str, content: dict):
    """
    将字典内容写入文件

    Args:
        filename (str): 文件名
        content (dict): 要写入的字典

    Returns:
        None
    """
    newline = ''  # 要写入的内容
    for k, v in content.items():
        newline += str(k) + ': ' + str(v) + '\t'
    newline += '\n'
    append_to_file(filename, newline)


def append_to_file(filename: str, content: str):
    """
    将字符串写入文件

    Args:
        filename (str): 文件名
        content (str): 要写入的字符串

    Returns:
        None
    """
    with open(filename, mode='r+', encoding='UTF-8') as file_object:
        file_object.seek(0, 2)
        file_object.writelines(content)


def plot_tuning_result(result_frame: pd.DataFrame):
    """
    绘制调参结果的热力图

    Args:
        result_frame (pd.DataFrame): 调参结果

    Returns:
        None
    """
    fig, ax = plt.subplots(figsize=(10, 10))
    # sns.set()
    result_frame = result_frame.pivot("c", "g", "Accuracy")
    hm = sns.heatmap(result_frame, ax=ax, cmap="YlGnBu")
    hm.set_xlabel(hm.get_xlabel(), labelpad=0, rotation=0)
    plt.yticks(rotation=0)
    plt.savefig('parameter heat map.png', dpi=260)


def draw_initial_point(label, data, ifDraw = True):
    x1 = [data1[1] for data1 in data]
    x2 = [data1[2] for data1 in data]
    if ifDraw:
     for i in range(len(label)):
        if label[i]== 1:
            plt.scatter(x1[i], x2[i], c='y', s=20,marker='o')
        else:
            plt.scatter(x1[i], x2[i], c='b', s=20,marker="o")
    return x1, x2
    # plt.title("data distribution")
    # plt.grid(True)  

def result_draw(label,data,svs):
    x1, x2 = draw_initial_point(label, data,False)
    x1_min,x2_min =np.asarray( x1).min()-0.1,np.asarray( x2).min()-0.1
    x1_max, x2_max = np.asarray(x1).max()+0.1, np.asarray(x2).max()+0.1
    N ,M = 100,100
    x_axis = np.linspace(x1_min, x1_max,N)
    y_axis = np.linspace(x2_min, x2_max,M)
    grid_x, grid_y = np.meshgrid(x_axis, y_axis)  # 生成网格采样点
    grid = np.stack([grid_x.flat, grid_y.flat], axis=1)  # 测试点
    y_predict, _, _ = svm_predict([], grid, model)
    my_color = matplotlib.colors.ListedColormap(['#A0FFA0', '#FFA0A0'])
    plt.pcolormesh(grid_x, grid_y, np.array(y_predict).reshape(grid_x.shape), cmap=my_color)
    for i in range(len(svs)):
        plt.scatter(svs[i][1], svs[i][2], s=100, c='r', alpha=0.5, linewidth=1.5, edgecolor='red')
    for i in range(len(label)):
        if label[i]== 0:
            plt.scatter(x1[i], x2[i], c='b', s=20,marker='o')
        else:
            plt.scatter(x1[i], x2[i], c='w', s=20,marker="o")
    
    
    

if __name__ =="__main__":  

   #train_lable, train_value = svm_read_problem("machine_learning/SVM/data.txt")
    data_lable, data_value = svm_read_problem("machine_learning/SVM/data.txt")
    draw_initial_point(data_lable,data_value)
    plt.title("data distribution")
    plt.grid(True)  
    plt.show()
    train_value, test_value, train_lable, test_lable = train_test_split(data_value,data_lable,test_size = 0.3)
    best_gauss_result, gauss_result_frame = tuning_gauss(train_value, train_lable, np.linspace(1, 10, int((10 - 1) * 1) + 1),np.linspace(0, 128, int((128 - 0) * 1) + 1))
    best_gauss_result, gauss_result_frame = tuning_gauss(train_value, train_lable, np.logspace(-4, 4, num=513, base=10),np.logspace(-4, 4, num=513, base=10))
    plot_tuning_result(gauss_result_frame)

    #线性
    # model = svm_train(train_lable, train_value,'-t 0 -c 46 ')
    #Gauss
    model = svm_train(train_lable, train_value,'-t 2 -c 1 -g 100 ')
    rho = model.rho[0] 
    svs = model.get_SV()
    sv_coeffs = model.get_sv_coef()  #alpha*y
    p_label, p_acc, p_val = svm_predict(test_lable, test_value, model)

    result_draw(train_lable,train_value,svs)
    plt.title("result")
    plt.grid(True)  
    plt.show()
    
    


