from genericpath import exists
import numpy as np
import os 

    
def svm_data_read(adress):
    if not os.path.exists(adress):
        assert("path does not exist")
    else:
        with open(adress) as f:
            lable = []
            data1 = []
            data =[]
            line_number = Get_Line_Number(adress)
            for i in range(line_number):
                worlds = f.readline().split()
                lable.append(eval(worlds[0]))
                data1 = {eval(worlds[1][0]):eval(worlds[1][2:]), eval(worlds[2][0]):eval(worlds[2][2:])}
                data.append(data1)
            return lable, data
                
                
            
def Get_Line_Number(file_path):
     count = -1
     for count, line in enumerate(open(file_path ,'rU')):
               pass
     count += 1
     return count

if __name__=="__main__":
     svm_data_read("machine_learning/SVM/data.txt")