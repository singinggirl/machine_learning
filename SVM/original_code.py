# -*- coding: utf-8 -*-
import numpy as np
from data_get import*
from svm import draw_initial_point
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import random
import math
from sklearn.model_selection import train_test_split

class SVM:
    def __init__(self,  data, lable,max_iteration = 1000, kernal_type = 'linear', c =100 ,t = 0.0001):
        self.max_iteration = max_iteration
        x1 = [data1[1] for data1 in data]
        x2 = [data1[2] for data1 in data]
        X = np.mat(np.c_[x1,x2])
        self.kernal_type =  kernal_type  #和函数类型
        self.initial_data = data     #初始数据（没有转变格式）
        self.data = X  #转变了格式的数据(我懒了，不想经常换格式)
        self.lable = lable #标签
        self.dimension = len(self.lable)     #有多少数据
        self.eStore= np.mat(np.zeros((self.dimension,2)))  #存储误差函数E
        self.b = 0 #偏移量b
        self.alphas = np.zeros(self.dimension) #参数alpha
        self.c = c      #常数C
        self.t = t  #容错
        self.kernals = np.mat(np.zeros((self.dimension,self.dimension))) #核矩阵
        self.sigma = 1     #高斯核函数的参数
        for i in range(self.dimension):      
            self.kernals[:,i] = self. kernal_chose(self.kernal_type,self.sigma,self.data, self.data[i,:])
  
    ##alpha选择 
    #最大方向或随机
    def   alpha_select(self,i, Ei):
       maxK = -1
       maxDeltaE = 0
       Ej = 0
       self.eStore[i] = [1,Ei]  
       valide =  np.where(self.eStore[:,0]!=0)[0]        #选有价值的（不为0）
       if (len(valide)) > 1:       #选一个差最大的
            for k in valide:   
                if k == i: 
                    continue 
                Ek = self.get_Ei( k)
                deltaE = abs(Ei - Ek)
                if (deltaE > maxDeltaE):
                    maxK = k
                    maxDeltaE = deltaE
                    Ej = Ek
            return maxK, Ej
       else:   #随机选择一个和i不同的
            j = i
            while (j==i):
                 j = int(random.uniform(0,self.dimension))
            Ej = self.get_Ei( j)
       return j, Ej
    
  #误差计算
    def get_Ei(self, k):
       F  = float(np.multiply(self.alphas ,np.asarray(self.lable).T).T *self.kernals[:, k]  + self.b)
       Ek =F - float(self.lable[k])
       return Ek

    ###核函数（目前只写了线性、高斯）
     
    def kernal_chose (self, type, sigma,X1, X2):
         K=np.zeros((self.dimension,1))
         if type == 'linear':
             K=  X1 * X2.T
         elif type == 'RBF':
             for i in range(self.dimension):
                 K[i] = (X1[i,:]-X2)*(X1[i,:]-X2).T
             K = np.exp(-K/(2*sigma**2))
         return K
 #限制alpha的范围     
    def Limit_alpha(self,alpha,L,H):
          if alpha <=L:
              return L
          elif alpha >=H:
              return H
          else:
              return alpha
 ##SMO
    def SMO_iteration(self,i):
                  Ei = self.get_Ei(i)
                  #选择不满足KKT条件的alpha
                  if (( self.lable[i] * Ei < -self.t) and (self.alphas[i] < self.c) ) or( ((self.lable[i]* Ei) > self.t) and(self.alphas[i]>0)):
                      j,  Ej = self.alpha_select(i, Ei) #根据i选择j
                    #保存更新前的alpha_i和alpha_j
                      alphai_before = self.alphas[i].copy()
                      alphaj_before = self.alphas[j].copy()
                      if (self.lable[i] != self.lable[j]):   #确定alpha2上下界
                          L = max(0, self.alphas[j]-self.alphas[i])
                          H = min(self.c, self.c +self.alphas[j] -self.alphas[i])
                      elif (self.lable[i] == self.lable[j]):                     
                         L = max(0, self.alphas[j] + self.alphas[i] - self.c)
                         H = min(self.c, self.alphas[j] +self.alphas[i])
                      if L==H: 
                         return 0
                      eta =self.kernals[i,i] - 2*self.kernals[i,j] + self.kernals[j,j]
                      if eta <=0:
                        return 0
                    #更新alpha2
                      self.alphas[j]+=self.lable[j]*(Ei-Ej)/eta
                      self.alphas[j]  = self.Limit_alpha(self.alphas[j],L,H)
                      Ek =self.get_Ei ( j)
                      self.eStore[j] = [1,Ek]
                      if (abs(self.alphas[j] - alphaj_before) < 0.00001): 
                          return 0
                      #更新alpha1
                      self.alphas[i] += self.lable[j]*self.lable[i]*(alphaj_before -self.alphas[j])
                      Ek =self.get_Ei ( i)
                      self.eStore[i] = [1,Ek]
                    #更新b
                      b1 = -Ei + self.b + (alphai_before-self.alphas[i])*self.lable[i]*self.kernals[i,i] + (alphaj_before-self.alphas[j])*self.lable[j]*self.kernals[i,j]
                      b2 = -Ej + self.b + (alphaj_before-self.alphas[j])*self.lable[j]*self.kernals[j,j] + (alphai_before-self.alphas[i])*self.lable[i]*self.kernals[i,j] 
                      if ( 0 < self.alphas[i]) and (self.alphas[i]<self.c):
                         self.b = b1
                      elif (0 < self.alphas[j]) and (self.alphas[j]< self.c):
                          self.b = b2
                      else:
                          self.b = (b1+b2)/2
                          return 1
                  else:
                        return 0
                          
         
    def SMO( self):
        iter = 0     
        if_entire = True  #是否遍历所有
        alpha_changed = 0   
        while (iter <   self.max_iteration) and ((alpha_changed > 0) or (if_entire)): 
            alpha_changed = 0     
            if if_entire:  
              for i in range(self.dimension):
                    alpha_changed += self.SMO_iteration(i) 
              iter += 1
              print ("fullSet, iter: %d i:%d, pairs changed %d" % (iter,i,alpha_changed))
            else:
             nonBoundIs = np.nonzero((np.mat(self.alphas).A > 0) * (np.mat(self.alphas).A < self.c))[0]
             for i in nonBoundIs:
                     alpha_changed += self.SMO_iteration(i)
                     print ("non-bound, iter: %d i:%d, pairs changed %d" % (iter,i,alpha_changed))
                     iter += 1
            if if_entire: 
               if_entire = False 
            elif (alpha_changed == 0): 
                if_entire= True  
                print ("iteration number: %d" % iter)

        svs = [] 
        for i, alpha in enumerate(self.alphas):
                if abs(alpha) > 0:
                    svs.append(self.initial_data[i])
        return self.b,self.alphas,svs           

#获取权重(如果为线性)
    def get_W(self):
        m,n =self.data.shape
        w = np.zeros(n)
        for i in range(self.dimension):
             w += self.alphas[i]*self.lable[i]*self.data[i].T
        return w

# def predict(solution, svs, data,lable,type,sigma):
#     m,n =data.shape()
#     x1 = [data1[1] for data1 in data]
#     x2 = [data1[2] for data1 in data]
#     X = np.mat(np.c_[x1,x2])
#     if type=='RBF':
#        for i in range(m):
#           kernelEval = solution. kernal_chose (type, sigma,svs,X[i,:]) 
#           predict=kernelEval.T * np.multiply(labelSV,alphas[svInd]) + b
 
 
#         if sign(predict)!=sign(labelArr[i]): errorCount += 1

    
        
def plot_fig(train_lable,train_value,svs):
    x1, x2 = draw_initial_point(train_lable,train_value,False)
    x1_min,x2_min =np.asarray( x1).min()-0.1,np.asarray( x2).min()-0.1
    x1_max, x2_max = np.asarray(x1).max()+0.1, np.asarray(x2).max()+0.1
    N ,M = 100,100
    x_axis = np.linspace(x1_min, x1_max,N)
    y_axis = np.linspace(x2_min, x2_max,M)
    grid_x, grid_y = np.meshgrid(x_axis, y_axis)  # 生成网格采样点
    grid = np.stack([grid_x.flat, grid_y.flat], axis=1)  # 测试点
    # y_predict, _, _ = svm_predict([], grid, model)
    # my_color = matplotlib.colors.ListedColormap(['#A0FFA0', '#FFA0A0'])
    # plt.pcolormesh(grid_x, grid_y, np.array(y_predict).reshape(grid_x.shape), cmap=my_color)
    for i in range(len(svs)):
        plt.scatter(svs[i][1], svs[i][2], s=100, c='r', alpha=0.5, linewidth=1.5, edgecolor='red')
    for i in range(len(train_lable)):
        if train_lable[i]== 1:
            plt.scatter(x1[i], x2[i], c='y', s=20,marker='o')
        else:
            plt.scatter(x1[i], x2[i], c='b', s=20,marker="o")
            
            

if __name__ =="__main__":  
  #阅读数据
    data_lable, data_value= svm_data_read("machine_learning/SVM/data_origin.txt")
    #画出原始数据分布
    draw_initial_point(data_lable, data_value)
    plt.title("data distribution")
    plt.grid(True)  
    plt.show()
    #获取测试集与训练集
    train_value, test_value, train_lable, test_lable = train_test_split(data_value,data_lable,test_size = 0.3)
    solution_svm = SVM( train_value, train_lable,max_iteration = 40, kernal_type = 'linear', c =1, t = 0)
    #获取偏移b,系数alpha以及支持向量svs
    b,alpha, svs = solution_svm.SMO()
    #绘制结果
    plot_fig(train_lable,train_value,svs)
    plt.title("result")
    plt.grid(True)  
    plt.show()
    

    